#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#ifdef _WIN32
#include <windows.h>
#define FOREGROUND_CYAN 0x0003
#define FOREGROUND_WHITE 0x0007
#else
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"
#endif

#define SUDOKU_SIZE 9
#define SQUARE_SIZE 3
#define MAX_NUMBER_INPUT 6
#define MAX_INT_INPUT 3


const int difficulties[3] = {30, 45, 60}; // Numbers to remove from easiest to hardest difficulty.

struct Game
{
	int board[SUDOKU_SIZE][SUDOKU_SIZE];
	int canWrite[SUDOKU_SIZE][SUDOKU_SIZE];
};


struct Game initGame(int);
void displayGameBoard(struct Game*);
void setNumber(struct Game*);
int getDifficulty();
int askReplay();
int isGameWon(struct Game*);
int isGameWonH(struct Game*);
int isGameWonV(struct Game*);
int isGameWonS(struct Game*);
int* getRandomArray();
void seedGame(struct Game*);
int solve(struct Game*, size_t, size_t);
int isValidMove(struct Game*, size_t, size_t, int);
void digHoles(struct Game*, int);
char upper(char);
int toInt(char);
int alphaToInt(char);


int main()
{
	srand((unsigned int) time(NULL));

	puts("Welcome to SupSudoku!\n");

	do
	{
		struct Game game = initGame(getDifficulty());

		do
		{
			displayGameBoard(&game);
			setNumber(&game);
		} while (!isGameWon(&game));

		displayGameBoard(&game);
		puts("\nCongratulation! You won the game!\n");
	} while (askReplay());

	return 0;
}


struct Game initGame(int difficulty)
{
	struct Game game;

	for (size_t y = 0; y < SUDOKU_SIZE; y++)
	{
		memset(game.board[y], 0, sizeof game.board[y]); // Sets all values to 0
		memset(game.canWrite[y], 0, sizeof game.canWrite[y]); // Everything is readOnly
	}

	seedGame(&game);
	solve(&game, 0, 0);
	digHoles(&game, difficulties[difficulty]);
	
	return game;
}


void displayGameBoard(struct Game* game)
{
	#ifdef _WIN32
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	#endif

	printf("\n  ");

	for (int i = 0; i < SUDOKU_SIZE; i++)
	{
		if (i != 0 && i % 3 == 0) printf(" |");

		#ifdef _WIN32
		SetConsoleTextAttribute(hConsole, FOREGROUND_GREEN);
		#else
		printf(ANSI_COLOR_GREEN);
		#endif

		printf(" %c", i + 65);

		#ifdef _WIN32
		SetConsoleTextAttribute(hConsole, FOREGROUND_WHITE);
		#else
		printf(ANSI_COLOR_RESET);
		#endif
	}

	puts("");

	for (size_t y = 0; y < SUDOKU_SIZE; y++)
	{
		if (y % 3 == 0) puts("  |---------------------|");

		#ifdef _WIN32
		SetConsoleTextAttribute(hConsole, FOREGROUND_CYAN);
		#else
		printf(ANSI_COLOR_CYAN);
		#endif

		printf("%zu ", y + 1);

		#ifdef _WIN32
		SetConsoleTextAttribute(hConsole, FOREGROUND_WHITE);
		#else
		printf(ANSI_COLOR_RESET);
		#endif

		for (size_t x = 0; x < SUDOKU_SIZE; x++)
		{
			if (x % 3 == 0) {
				printf("|");

				if (x != 0) printf(" ");
			}

			int number = game->board[y][x];
			int canWrite = game->canWrite[y][x];

			if (canWrite)
			{
				#ifdef _WIN32
				SetConsoleTextAttribute(hConsole, FOREGROUND_RED);
				#else
				printf(ANSI_COLOR_RED);
				#endif
			}

			(number > 0) ? printf("%d", number) : printf(".");

			if (canWrite)
			{
				#ifdef _WIN32
				SetConsoleTextAttribute(hConsole, FOREGROUND_WHITE);
				#else
				printf(ANSI_COLOR_RESET);
				#endif
			}

			if (x != SUDOKU_SIZE - 1) printf(" ");
		}

		puts("|");
	}

	puts("  |---------------------|\n");
}


void setNumber(struct Game* game)
{
	#ifdef _WIN32
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	#endif

	while (1) {
		char input[MAX_NUMBER_INPUT];
		fseek(stdin, 0, SEEK_END); // Clears stdin

		printf("Please enter a number (");

		#ifdef _WIN32
		SetConsoleTextAttribute(hConsole, FOREGROUND_GREEN);
		#else
		printf(ANSI_COLOR_GREEN);
		#endif

		printf("Col");

		#ifdef _WIN32
		SetConsoleTextAttribute(hConsole, FOREGROUND_CYAN);
		#else
		printf(ANSI_COLOR_CYAN);
		#endif

		printf("Row");

		#ifdef _WIN32
		SetConsoleTextAttribute(hConsole, FOREGROUND_WHITE);
		#else
		printf(ANSI_COLOR_RESET);
		#endif

		printf(" Num): ");

		fgets(input, MAX_NUMBER_INPUT, stdin);

		char collumn = upper(input[0]);
		
		if (collumn < 'A' || collumn > 'I')
		{
			puts("\nInvalid collumn, try again.\n");
			continue;
		}

		char row = input[1];

		if (row < '1' || row > '9')
		{
			puts("\nInvalid row, try again.\n");
			continue;
		}

		char number = input[3];

		if (number < '0' || number > '9')
		{
			puts("\nInvalid number, try again.\n");
			continue;
		}

		int iCollumn = alphaToInt(collumn) - 1;
		int iRow = toInt(row) - 1;
		int iNumber = toInt(number);

		if (game->canWrite[iRow][iCollumn])
		{
			game->board[iRow][iCollumn] = iNumber;
			break;
		}
		else
		{
			puts("\nYou cannot rewrite this number, choose another one.\n");
		}
	}
}


int getDifficulty()
{
	while (1) {
		char input[MAX_INT_INPUT];
		fseek(stdin, 0, SEEK_END); // Clears stdin

		printf("Choose the difficulty (1, 2, 3): ");
		fgets(input, MAX_INT_INPUT, stdin);

		char difficulty = input[0];

		if (difficulty < '1' || difficulty > '3')
		{
			puts("\nInvalid difficulty, try again.\n");
			continue;
		}

		return toInt(difficulty) - 1;
	}
}


int askReplay()
{
	while (1) {
		char input[MAX_INT_INPUT];
		fseek(stdin, 0, SEEK_END); // Clears stdin

		printf("Do you wish to play again? [Y/N]: ");
		fgets(input, MAX_INT_INPUT, stdin);

		char choice = upper(input[0]);

		if (choice == 'Y') return 1;
		else if (choice == 'N') return 0;
	}
}


int isGameWon(struct Game* game)
{
	if (
		isGameWonH(game) &&
		isGameWonV(game) &&
		isGameWonS(game)
	) return 1;
	
	return 0;
}


int isGameWonH(struct Game* game)
{
	for (size_t y = 0; y < SUDOKU_SIZE; y++)
	{
		int numbers[SUDOKU_SIZE];
		memset(numbers, 0, sizeof numbers); // Sets all values to 0

		for (size_t x = 0; x < SUDOKU_SIZE; x++)
		{
			int number = game->board[y][x];
			if (!number || numbers[number - 1]) return 0;
			
			numbers[number - 1] = 1;
		}
	}

	return 1;
}


int isGameWonV(struct Game* game)
{
	for (size_t x = 0; x < SUDOKU_SIZE; x++)
	{
		int numbers[SUDOKU_SIZE];
		memset(numbers, 0, sizeof numbers); // Sets all values to 0

		for (size_t y = 0; y < SUDOKU_SIZE; y++)
		{
			int number = game->board[y][x];
			if (!number || numbers[number - 1]) return 0;

			numbers[number - 1] = 1;
		}
	}

	return 1;
}


int isGameWonS(struct Game* game)
{
	for (size_t yStart = 0; yStart < SUDOKU_SIZE; yStart += 3)
	{
		for (size_t xStart = 0; xStart < SUDOKU_SIZE; xStart += 3)
		{
			int numbers[SUDOKU_SIZE];
			memset(numbers, 0, sizeof numbers); // Sets all values to 0

			for (size_t y = 0; y < SQUARE_SIZE; y++)
			{
				for (size_t x = 0; x < SQUARE_SIZE; x++)
				{
					int number = game->board[yStart + y][xStart + x];
					if (!number || numbers[number - 1]) return 0;

					numbers[number - 1] = 1;
				}
			}
		}
	}

	return 1;
}


int* getRandomArray()
{
	int* array = malloc(sizeof(int) * SUDOKU_SIZE);
	int used[SUDOKU_SIZE];
	memset(used, 0, sizeof used); // Sets all values to 0

	for (size_t i = 0; i < SUDOKU_SIZE; i++)
	{
		while (1)
		{
			int number = rand() % SUDOKU_SIZE + 1;

			if (!used[number - 1])
			{
				array[i] = number;
				used[number - 1] = 1;
				break;
			}
		}
	}

	return array;
}


void seedGame(struct Game* game)
{
	int* seeds[3];
	for (size_t i = 0; i < 3; i++) seeds[i] = getRandomArray();
	
	for (size_t i = 0; i < SQUARE_SIZE; i++)
	{
		size_t startPos = i * SQUARE_SIZE;
		size_t numIndex = 0;

		for (size_t yOffset = 0; yOffset < SQUARE_SIZE; yOffset++)
		{
			for (size_t xOffset = 0; xOffset < SQUARE_SIZE; xOffset++)
			{
				game->board[startPos + yOffset][startPos + xOffset] = seeds[i][numIndex];
				numIndex++;
			}
		}
	}

	for (size_t i = 0; i < 3; i++) free(seeds[i]);
}


int solve(struct Game* game, size_t y, size_t x)
{
	if (y < SUDOKU_SIZE && x < SUDOKU_SIZE)
	{
		if (game->board[y][x] != 0)
		{
			if (x < SUDOKU_SIZE - 1)
				return solve(game, y, x + 1);
			else if (y < SUDOKU_SIZE - 1)
				return solve(game, y + 1, 0);
			else
				return 1;
		}
		else
		{
			int* numbers = getRandomArray();

			for (int i = 0; i < SUDOKU_SIZE; i++)
			{
				int number = numbers[i];

				if (isValidMove(game, y, x, number))
				{
					game->board[y][x] = number;

					if (x < SUDOKU_SIZE - 1)
					{
						if (solve(game, y, x + 1))
						{
							free(numbers);
							return 1;
						}
						else game->board[y][x] = 0;
					}
					else if (y < SUDOKU_SIZE - 1)
					{
						if (solve(game, y + 1, 0))
						{
							free(numbers);
							return 1;
						}
						else game->board[y][x] = 0;
					}
					else
					{
						free(numbers);
						return 1;
					}
				}
			}
		}
		
		return 0;
	}

	return 1;
}


int isValidMove(struct Game* game, size_t y, size_t x, int number)
{
	for (size_t i = 0; i < SUDOKU_SIZE; i++)
	{
		if (game->board[y][i] == number || game->board[i][x] == number) return 0;
	}

	size_t yStart = (size_t) floor((double) y / SQUARE_SIZE) * SQUARE_SIZE;
	size_t xStart = (size_t) floor((double) x / SQUARE_SIZE) * SQUARE_SIZE;

	for (size_t y = 0; y < SQUARE_SIZE; y++)
	{
		for (size_t x = 0; x < SQUARE_SIZE; x++)
		{
			if (game->board[yStart + y][xStart + x] == number) return 0;
		}
	}

	return 1;
}


void digHoles(struct Game* game, int holes)
{
	for (int i = 0; i < holes;)
	{
		int y = rand() % SUDOKU_SIZE;
		int x = rand() % SUDOKU_SIZE;

		if (game->board[y][x])
		{
			game->board[y][x] = 0;
			game->canWrite[y][x] = 1;
			i++;
		}
	}
}


char upper(char chr)
{
	if (chr >= 'a' && chr <= 'z')
	{
		return chr - 32;
	}

	return chr;
}


int toInt(char chr)
{
	return chr - 48;
}


int alphaToInt(char chr)
{
	return chr - 64;
}
